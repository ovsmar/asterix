use asterix_db;


/*1. Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)*/
SELECT * FROM potion;

/*2. Liste des noms des trophées rapportant 3 points. (2 lignes)*/
SELECT NomCateg FROM categorie WHERE NbPoints = 3;

/*3. Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)*/
SELECT NomVillage FROM village WHERE NbHuttes > 35;

/*4. Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)*/
SELECT NumTrophee FROM trophee WHERE DatePrise > cast("2052-05-01 00:00:00" AS DATETIME)
AND DatePrise < cast("2052-06-30 00:00:00" AS DATETIME);

/*5. Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)*/
SELECT Nom FROM habitant WHERE Nom Like 'a%' AND Nom Like '%r%';

/*6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)*/
SELECT DISTINCT NumHab FROM absorber WHERE NumPotion = 1 OR NumPotion = 3 OR NumPotion = 4;

/*7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10
lignes)*/
SELECT NumTrophee, DatePrise, NomCateg, Nom FROM trophee INNER JOIN categorie ON trophee.CodeCat = categorie.CodeCat INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab;

/*8. Nom des habitants qui habitent à Aquilona. (7 lignes)*/
SELECT Nom FROM habitant INNER JOIN village WHERE habitant.NumVillage = village.NumVillage AND village.NomVillage = "AquilONa";

/*9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)*/
SELECT Nom FROM habitant INNER JOIN trophee ON habitant.NumHab = trophee.NumPreneur INNER JOIN categorie ON trophee.CodeCat = categorie.CodeCat WHERE categorie.NomCateg = "Bouclier de Légat";

/*10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
principal. (3 lignes)*/
SELECT LibPotion FROM potion INNER JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion INNER JOIN habitant ON fabriquer.NumHab = habitant.NumHab WHERE habitant.Nom = "Panoramix";

/*11. Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)*/
SELECT DISTINCT LibPotion FROM potion INNER JOIN absorber ON potion.NumPotion = absorber.NumPotion INNER JOIN habitant ON absorber.NumHab = habitant.NumHab WHERE habitant.Nom = "Homeopatix";

/*12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro
3. (4 lignes)*/
SELECT DISTINCT Nom FROM habitant INNER JOIN absorber ON habitant.NumHab = absorber.NumHab INNER JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion WHERE fabriquer.NumHab = 3;

/*13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)*/
select nom from habitant 
where numhab in (
    select numhab from absorber 
    where numpotion in (
        select numpotion FROM fabriquer 
        where numhab = (
            select numhab from habitant where
            nom LIKE 'amnésix'
        ) 
    )
);

/*14. Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)*/
SELECT NOM FROM habitant WHERE NumQualite is NULL;

/*15. Nom des habitants ayant absorbé la potion magique n°1 (c'est le libellé de la
potion) en février 52. (3 lignes)*/
SELECT nom from habitant , absorber where absorber.NumPotion = 1 and SUBSTR(dateA, 1, 7) = "2052-02" AND habitant.NumHab = absorber.NUMHAB;

/*16. Nom et âge des habitants par ordre alphabétique. (22 lignes)*/
SELECT Nom, Age FROM habitant ORDER BY Nom;

/*17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom
du village. (3 lignes)*/
SELECT NomResserre, NomVillage FROM resserre INNER JOIN village ON resserre.NumVillage = village.NumVillage ORDER BY resserre.NomResserre DESC;

/*18. Nombre d'habitants du village numéro 5. (4)*/
SELECT COUNT(*) FROM habitant WHERE NumVillage = 5;

/*19. Nombre de points gagnés par Goudurix. (5)*/
SELECT sum(NbPoints) FROM categorie INNER JOIN trophee ON categorie.CodeCat = trophee.CodeCat INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab WHERE habitant.Nom = "Goudurix";

/*20. Date de première prise de trophée. (03/04/52)*/
SELECT MIN(DatePrise) FROM trophee ;

/*21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)*/
SELECT sum(Quantite) FROM absorber INNER JOIN potion ON absorber.NumPotion = potion.NumPotion WHERE potion.LibPotion = "potion magique n°2";

/*22. Superficie la plus grande. (895)*/
SELECT MAX(Superficie) FROM resserre;

/*23. Nombre d'habitants par village (nom du village, nombre). (7 lignes)*/
SELECT NomVillage , Count(*) FROM habitant INNER JOIN village ON habitant.NumVillage = village.NumVillage group by habitant.NumVillage ;

/*24. Nombre de trophées par habitant (6 lignes)*/
SELECT Nom, COUNT(*) FROM trophee INNER JOIN habitant ON trophee.NumPreneur = habitant.NumHab GROUP BY habitant.Nom;

/* 25. Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes) */
SELECT NomProvince, AVG(Age) FROM province INNER JOIN village ON province.NumProvince = village.NumProvince INNER JOIN habitant ON village.NumVillage = habitant.NumVillage GROUP BY NomProvince;

/*26. Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9
lignes)*/
Select Nom, COUNT(*) FROM potion INNER JOIN absorber on potion.NumPotion = absorber.NumPotion INNER JOIN habitant on habitant.NumHab = absorber.NumHab GROUP BY habitant.Nom;

/*27. Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)*/
select nom from habitant, absorber, potion where habitant.NumHab = absorber.NumHab and absorber.NumPotion = potion.NumPotion AND potion.LibPotion = "Potion Zen" AND  absorber.Quantite > 2 ;

/*28. Noms des villages dans lesquels on trouve une resserre (3 lignes)*/
SELECT NomVillage From village INNER JOIN resserre on village.NumVillage = resserre.NumVillage;

/*29. Nom du village contenant le plus grand nombre de huttes. (Gergovie)*/
SELECT NomVillage FROM village WHERE NbHuttes = (SELECT MAX(NbHuttes) FROM village);

/*30. Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).*/

SELECT nom FROM (
    SELECT nom, COUNT(*) as nb FROM habitant 
    INNER JOIN trophee on trophee.numpreneur = habitant.numhab 
    GROUP BY trophee.numpreneur
    ) as nom_nb 
    WHERE nb > (
        SELECT COUNT(*) as nb FROM habitant 
        INNER JOIN trophee on trophee.numpreneur = habitant.numhab
        WHERE habitant.nom LIKE 'Obélix'
        GROUP BY trophee.numpreneur
    );
